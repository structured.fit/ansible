FROM williamyeh/ansible:alpine3

RUN apk --no-cache add --virtual build-dependencies \
    openssl \
    libc-dev \
    python-dev \
    gcc \
  && pip install \
    dopy \
  && apk del build-dependencies